import random

num = int(input(f'Select the game that you want proof \n [1] Guess \n [2] Computer Guess'))
x = int(input(f'Select the max number for guess number'))

def guess(x):
  r_number = random.randint(1, x)
  
  while guess != r_number:
    guess = int(input(f'Guess a number between 1 and {x}'))
    print(guess)
    if guess < r_number:
      print('Sorry, guess again. Too low')
    elif guess > r_number:
      print('Sorry, guess again. Too high')
      
  print(f'Yay, congrats. You have guessed the number {r_number} correctly!!!')

def computer_guess(x):
  low = 1
  high = x
  feedback = ''
  while feedback != 'c':
    if low != high:
      guess = random.randint(low, high)
    else:
      guess = low
    feedback = input(f'Is {guess} too high (H), too low (L), or correct (C)')
    if feedback == 'h':
      high = guess -1
    elif feedback == 'l':
      low = guess + 1
    
  print(f'Yay!! The computer guessed your number, {guess}, correctly!!!')

def switch(num):
  if num == 1:
    guess(x)
  elif num == 2:
    computer_guess(x)

switch(num)
